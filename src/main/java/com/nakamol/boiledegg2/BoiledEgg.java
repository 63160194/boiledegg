/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.boiledegg2;

/**
 *
 * @author OS
 */
public class BoiledEgg {

    private int minute;
    private int heat;
    private int numegg;


    public BoiledEgg(int minute, int heat, int numegg) {
        this.minute = minute;
        this.heat = heat;
        this.numegg = numegg;
    }

    public void calEgg() {
        if (numegg < 4) {
            if (minute >= 0 && minute <= 1) {
                if (heat >= 0 && heat <= 20) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Eggs are still raw!!!");
                } else if (heat >= 21 && heat <= 40) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Egg whites begin to cook!!!");
                } else if (heat >= 41 && heat <= 60) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Cook egg whites!!!");
                } else if (heat >= 61 && heat <= 80) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Cooked egg yolk!!!");
                } else if (heat >= 81 && heat <= 100) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Boiled egg!!!");
                }
            }
            if (minute >= 2 && minute <= 3) {
                if (heat >= 0 && heat <= 20) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Egg whites begin to cook!!!");
                } else if (heat >= 21 && heat <= 40) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Cook egg whites!!!");
                } else if (heat >= 41 && heat <= 60) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " A soft boiled egg!!!");
                } else if (heat >= 61 && heat <= 80) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " An onsen egg!!!");
                } else if (heat >= 81 && heat <= 100) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Boiled egg!!!");
                }
            }
            if (minute >= 5 && minute <= 7) {
                if (heat >= 0 && heat <= 20) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " A soft boiled egg!!!");
                } else if (heat >= 21 && heat <= 40) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " An onsen eggs!!!");
                } else if (heat >= 41 && heat <= 60) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Scrambled eggs!!!");
                } else if (heat >= 61 && heat <= 80) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Almost cooked egg!!!");
                } else if (heat >= 81 && heat <= 100) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Boiled egg!!!");
                }
            }
            if (minute >= 9 && minute <= 11) {
                if (heat >= 0 && heat <= 20) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Soft boiled egg!!!");
                } else if (heat >= 21 && heat <= 40) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Onsen egg!!!");
                } else if (heat >= 41 && heat <= 60) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Scrambled egg!!!");
                } else if (heat >= 61 && heat <= 80) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Almost cooked egg!!!");
                } else if (heat >= 81 && heat <= 100) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Boiled egg!!!");
                }
            }
            if (minute >= 13 && minute <= 15) {
                if (heat >= 0 && heat <= 20) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Scrambled egg!!!");
                } else if (heat >= 21 && heat <= 40) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Almost cooked egg!!!");
                } else if (heat >= 41 && heat <= 60) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Boiled egg!!!");
                } else if (heat >= 61 && heat <= 80) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Overcook egg!!!");
                } else if (heat >= 81 && heat <= 100) {
                    System.out.println("heat is: " + this.heat);
                    System.out.println("in time " + this.minute + " minute and use heat " + this.heat + " Exploding egg!!!");
                }
            }
        } else {
            System.out.println("The number of eggs exceeded the limit!!!");
        }
    }

    public int getMinute() {
        return minute;
    }

    public int getHeat() {
        return heat;
    }

    public int getNumegg() {
        return numegg;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setHeat(int heat) {
        this.heat = heat;
    }

    public void setNumegg(int numegg) {
        this.numegg = numegg;
    }

    public void showEgg() {
        System.out.println("egg boiling time: " + this.minute + " heat boiled eggs: " + this.heat + " number of eggs: " + this.numegg);
    }
}
